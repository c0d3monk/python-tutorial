
class MyException(Exception):

    def __init__(self, *args):
        print("calling init")
        if args:
            self.message = args[0]
        else:
            self.message = ''

    def __str__(self):
        print("calling str")
        if self.message:
            return "Here MyException: {0}".format(self.message)
        else:
            return "MyException had no custom message"

# raise MyException()

raise MyException('custom error msg')
