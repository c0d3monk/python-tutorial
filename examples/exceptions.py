
import sys

mydict = {'a': 1, 'b': 2}

i = input("Enter key to get value: ");

try:
    print("Key: {0} and value: {1}".format(i, mydict[i]))
except KeyError:
    raise KeyError("Error KeyError occured")

print('End of program')
