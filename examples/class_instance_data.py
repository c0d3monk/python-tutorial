#!/usr/bin/env python

class InstanceCounter():
    count = 0

    def __init__(self, val):
        self.val = val
        InstanceCounter.count += 1

    def set_val(self, newval):
        self.val = newval

    def get_val(self):
        return self.val

    def get_count(self):
        return InstanceCounter.count


a = InstanceCounter(1)
b = InstanceCounter(2)
c = InstanceCounter(3)

for i in (a, b, c):
    print("val of obj: %s" % (i.get_val()))
    print("count (from instance): %s" % (i.get_count()))
