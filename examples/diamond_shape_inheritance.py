#!/usr/bin/env python

class A():
    def doThis(self):
        print("In A")

class B(A):
    pass

class C(A):
    def doThis(self):
        print("In C")

class D(B, C):
    pass

d = D()
d.doThis() # In C

print(D.mro()) # [<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>]
