#!/usr/bin/env python

class GetSet():

    def __init__(self, val):
        self.internalval = val

    @property
    def var(self):
        print("getting the value")
        return self.internalval

    @var.setter
    def var(self, val):
        print("setting the value")
        self.internalval = val

    @var.deleter
    def var(self):
        print("deleting the value")
        self.internalval = None


x = GetSet(5)
x.var = 100

print(x.var)
del(x.var)
print(x.var)
