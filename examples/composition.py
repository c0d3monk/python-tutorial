#!/usr/bin/env python

import StringIO

class WriterClass():

    def __init__(self, writer):
        self.writer = writer

    def write(self):
        write_text = "example text"
        self.writer.write(write_text)

fh = open('text.txt', 'w')
w1 = WriterClass(fh)
w1.write()
fh.close

sioh = StringIO.StringIO()
w2 = WriterClass(sioh)
w2.write()

print("file object", open('text.txt', 'r').read())
print("StringIO object: ", sioh,getvalue())
