#!/usr/bin/env python

class Date(object):
    def get_date(self):
        return 'Called get_date'

class Time(Date):
    def get_time(self):
        return 'Called get_time'


if __name__ == '__main__':
    dt = Date()
    print(dt.get_date())

    tm = Time()
    print(tm.get_time())
    print(tm.get_date()) # Called the inherited function
