
class MyList(list):

    def __setitem__(self, index, value):
        if index == 0: raise IndexError
        if index > 0: index -= 1
        return list.__setitem__(self, index, value)


    def __getitem__(self, index):
        if index == 0: raise IndexError
        if index > 0: index -= 1
        return list.__getitem__(self, index)

x = MyList(['a', 'b', 'c'])

print(x)

x.append('spam')

print(x[1]) # Actually gets the item from 0th index

print(x[4]) # Actually get the item from 3rd index
