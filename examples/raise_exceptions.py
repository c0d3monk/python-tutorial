
import sys

mydict = {'a': 1, 'b': 2}

try:
    print(5/0)
except ZeroDivisionError as e:
    print(e.with_traceback)
    print(e.args)
