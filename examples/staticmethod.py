#!/usr/bin/env python

class InstanceCounter():
    count = 0

    def __init__(self, val):
        self.val = self.filterint(val)
        InstanceCounter.count += 1

    @staticmethod
    def filterint(value):
        if not isinstance(value, int):
            return 0
        else:
            return value


a = InstanceCounter(1)
b = InstanceCounter(2)
c = InstanceCounter(3)

print(a.val)
print(b.val)
print(c.val)
