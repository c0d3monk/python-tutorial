
class SumList():

    def __init__(self, list):
        self.list = list

    def __add__(self, other):
        new_list = [x + y for x, y in zip(self.list, other.list)]

        return SumList(new_list)

    def __repr__(self):
        return str(self.list)

a = SumList([1, 2])
b = SumList([100, 200])

c = a + b
print(c)
