#!/usr/bin/env python

class MyClass():

    def __enter__(self):
        print("entering with block")
        return self

    def __exit__(self, type, value, traceback):
        print('Error type: {0}'.format(type))
        print('Error value: {0}'.format(value))
        print('Error traceback: {0}'.format(traceback))
        print("exiting with block")


    def sayhi(self):
        print("saying hi!!")

with MyClass() as c:
    c.sayhi()
    4/0 # to illustrate the error being caught in __exit__
