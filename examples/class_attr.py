#!/usr/bin/env python

class MyClass():
    class_attr = 10



c = MyClass()
print(c.class_attr)     # print class attr value 10
c.class_attr = 11       # set the class attr directly using the instance
print(c.class_attr)     # 11
del c.class_attr        # del the instance attr
print(c.class_attr)     # print 10, the class attr value
