#!/usr/bin/env python

class Animal():
    def __init__(self, name):
        self.name = name

    def eat(self, food):
        print("%s is eating %s" % (self.name, food))

class Dog(Animal):
    def barks(self, thing):
        print("%s barks at %s" % (self.name, thing))

class Cat(Animal):
    def scratches(self, food):
        print("%s scratches the %s" % (self.name, food))

d = Dog('Rocky')
c = Cat('Ginger')

d.barks('tree')
c.scratches('pillow')
d.eat('corn')
c.eat('fish')
# d.scratches('somthine') # AttributeError: Dog instance has no attribute 'scratches'
