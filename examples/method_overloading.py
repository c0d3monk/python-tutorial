import abc

class Parent():
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.value = 0

    def set_val(self, value):
        self.value = value

    def get_val(self):
        return self.value

    @abc.abstractmethod
    def show(self):
        return

class OverloadingClass(Parent):
    def set_val(self, value):
        if not isinstance(value, int):
            value = 0
        else:
            super().set_val(value)

    def show(self):
        print("Overloading class {0}".format(id(self)))


class ExtendingClass(Parent):
    def __init__(self, value=0):
        self.vallist = [value]

    def get_val(self):
        return self.vallist[-1]

    def get_all_values(self):
        return self.vallist

    def set_val(self, value):
        self.vallist.append(value)

    def show(self):
        print("Extending class, length of vallist: {0}".format(len(self.vallist)))


x = OverloadingClass()
x.set_val(5)
print(x.get_val())
x.show()

y = ExtendingClass(6)
y.set_val(7)
y.set_val(8)
print("get_val output: %d" % (y.get_val()))
print("get_all_values")
print(y.get_all_values())

y.show()
