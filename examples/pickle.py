import pickle

class MyClass(object):

    def __init__(self, init_val):
        self.val = init_val

    def inc(self):
        self.val += 1

cc = MyClass(5)
cc.inc()
cc.inc()

with open('pickled.txt', 'w') as fh:
    pickle.dump(cc, fh)

with open('pickled.txt', 'r') as fh:
    unpickled_cc = pickle.load(fh)

print(unpickled_cc)
print(unpickled_cc.val)
