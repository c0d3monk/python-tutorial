#!/usr/bin/env python

class Animal():
    def __init__(self, name):
        self.name = name

    def eat(self, food):
        print "%s is eating %s" % (self.name, food)

class Dog(Animal):
    def does(self):
        print("%s waves the tail" % (self.name))

class Cat(Animal):
    def does(self):
        print("%s purrs" % (self.name))

d = Dog('Rocky')
c = Cat('Ginger')

d.does()
c.does()
