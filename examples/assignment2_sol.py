
import abc
from datetime import datetime

class Write():

    __metaclass__ = abc.ABCMeta

    def __init__(self, filename):
        self.filename = filename

    @abc.abstractmethod
    def write(self):
        return

    def write_line(self, msg):
        fh = open(self.filename, 'a')
        fh.write(msg + '\n')
        fh.close()

class LogFile(Write):

    def __init__(self, filename):
        super().__init__(filename)

    def write(self, msg):
        dt = datetime.now()
        date_str = dt.strftime('%Y-%m-%d %H:%M')
        self.write_line("{0}: {1}".format(date_str, msg))

class DelimFile(Write):

    def __init__(self, filename, delimiter=None):
        super().__init__(filename)
        self.delimiter = delimiter
        if not self.delimiter:
            self.delimiter = ','

    def write(self, list):
        line = self.delimiter.join(list)
        self.write_line(line)

log = LogFile('log.txt')
log.write('a log msg')
log.write('another log msg')

c = DelimFile('text.csv', ',')
c.write(['1', '2'])
c.write(['a', 'b'])
