#!/usr/bin/env python


class MaxSizeList():
    def __init__(self, val):
        self.max_size = val
        self.inner_list = []

    def push(self, val):
        if len(self.inner_list) < self.max_size:
            self.inner_list.append(val)

    def get_list(self):
        return self.inner_list

a = MaxSizeList(3)
b = MaxSizeList(1)

a.push("one")
a.push("two")
a.push("three")
a.push("four")
a.push("five")

b.push("one")
b.push("two")
b.push("three")
b.push("four")
b.push("five")

print(a.get_list())
print(b.get_list())
