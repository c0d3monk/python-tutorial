#!/usr/bin/env python

import abc

class GetterSetter():
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def set_val(self, val):
        return

    @abc.abstractmethod
    def get_val(self):
        return

    @abc.abstractmethod
    def extra(self):
        return

class ActualClass(GetterSetter):
    def set_val(self, val):
        self.val = val

    def get_val(self):
        return self.val


x = ActualClass()
x.set_val(5)
print(x.get_val())
