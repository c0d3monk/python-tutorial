import json

x = json.dumps({'a': ['alpha', 'beta', 'gamma'], 'b': ['x', 'y', 'z']})

print(x)

mystruct = json.loads(x)

for key in mystruct:
    print(key)
    for el in mystruct[key]:
        print(" {0}".format(el))
