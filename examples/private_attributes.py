#!/usr/bin/env python

class GetSet():

    instance_count = 0

    __mangled_name = 'no privacy!'

    def __init__(self, val):
        self._internalval = val
        GetSet.instance_count += 1

    @property
    def var(self):
        print("getting the value")
        return self._internalval

    @var.setter
    def var(self, val):
        print("setting the value")
        self._internalval = val

    @var.deleter
    def var(self):
        print("deleting the value")
        self._internalval = None


x = GetSet(5)
x.var = 100

print(x._internalval)
print(x._GetSet__mangled_name)
